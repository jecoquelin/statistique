drop schema if exists partie1 cascade;

create schema partie1;

set schema 'partie';
/*
WbImport -file=./data/ppn.csv
         -header=true
         -delimiter=';'
         -table=_individu
         -schema=partie2
         -filecolumns=$wb_skip$
;*/

WbImport -file=./data/v_candidatures.csv
         -header=true
         -delimiter=';'
         -table=_individu
         -schema=partie2
         -filecolumns=$wb_skip$,$wb_skip$,$wb_skip$,nom,prenom,sexe,date_naissance,nationalité,code_postal,ville,$wb_skip$,$wb_skip$,$wb_skip$,INE
;


id
nom                 
prenom              
date_naissance      
code_postale        
ville               
sexe                
nationalité        
INE                 
