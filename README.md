# Statistique

Ce dépôt contient des statistique sur une partie de base de données.

## Contexte
Lors du BUT informatique, nous avons eu des projets à réaliser. Celui-ci est l'un deux.

## Description

Cette SAE nous a permis de nous rendre compte du métier de data scientist en gérant beaucoup de données pour pouvoir faire des analyses statistiques par la suite. Ceci nous a permis d'appliquer nos connaissances en SQL dans un projet de plus grande envergure.

Étapes de cette SAE 2.04 :

1. Nous avons tout d'abord traduit un modèle en un schéma relationnel puis l'implémenter en postgree sql.
2. Ensuite, nous avons peuplé la base de données à partir de données réelles. Nous avons fait face à des modifications de ce schéma pour ne pas violer de contraintes de clé étrangère.
3. Après cela, nous avons créé des vues pour pouvoir exploiter ces données. Nous les avons ainsi mis sur un script python permettant de faire des analyses statistiques. Après cela, nous avons délivré un rapport précisant la problématique choisi et l'analyse que l'on a pu déduite a partir des données.
4. Après cela, nous avons présenté notre travail en faisant un oral en Anglais.

## Apprentissage critique

AC14.01 Mettre à jour et interroger une base de donnée relationnelle (en requêtes directes ou à travers une application)<br>
AC14.02 Visualiser des données<br>
AC14.03 Concevoir une base de donnée relationnelle à partir d’un cahier des charges

## Langage et Framework 
  * PostgreSql
  * Python

## Prérequis 
Afin de pouvoir exécuter l'application sur votre poste, vous devrez avoir :
  * python

## Exécution
Pour lancer le problème, il vous suffit de télécharger le site web et de lancer le fichier `pyton statistique`. 