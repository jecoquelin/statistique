import pandas as pd
import numpy as np

pokemons=pd.read_csv("pokemon.csv")


df=pd.DataFrame(pokemons._get_numeric_data())

PokArray = df.to_numpy()



def CentreReduire(array):
    
    x,y = array.shape
    tab = np.zeros( (x,y))
    moy = np.average(array,axis=0)
    ecartype = np.std(array,axis=0)
    for i in range(0,x):
        for j in range(0,y):
            tab[i][j] = (array[i][j] - moy[j]) / ecartype[j]
    return tab


#print(df)
#print(PokArray)
print("La moyenne de chaque colonnes")
print(np.average(PokArray,axis=0))
print("\nL'écart type de chaque colonnes")
print(np.std(PokArray,axis=0))
print("\nCentrer reduire")
print(CentreReduire(PokArray))
PokemonsCR =pd.DataFrame(CentreReduire(PokArray))
print("\nData frame centre Reduire")
print(PokemonsCR)

#print(pokemons._get_numeric_data())