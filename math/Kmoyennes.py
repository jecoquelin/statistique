# -*- coding: utf-8 -*-
"""
Created on Tue May 17 11:23:28 2022

@author: tiphaine
"""

import numpy as np
import numpy.random as rd
import pandas as pd


def Distance(L1,L2):
    #calcule la distance euclidienne entre 2 tableaux lignes de même taille
    p=len(L1)
    d=0
    for i in range(p):
        d+=(L1[i]-L2[i])**2
    return np.sqrt(d)

def Kmoyennes(Tab,N):
    (n,p)=Tab.shape
    
    #Initialisation des centroides avec individus aléatoires de Tab
    Centroids=np.zeros((N,p))
    rand=rd.randint(n,size=N)
    for k in range(N):
        Centroids[k]=Tab[rand[k]]
    
    for I in range(40):
        #Création du tableau des numéros de clusters
        Labels=np.zeros(n)
        Dist_i=np.zeros(N)
        for i in range(n):
            for k in range(N):
                Dist_i[k]=Distance(Tab[i],Centroids[k])
            Labels[i]=np.argmin(Dist_i)
        
        #Calcul des nouveaux centroides
        for k in range(N):
            Cluster_k=np.array([]).reshape(0,p)  #tableau qui contiendra les individus labellisés k
            for i in range(n):
                if Labels[i]==k:
                    Cluster_k=np.append(Cluster_k,[Tab[i]],axis=0)
            #print(Cluster_k)
            Centroids[k]=np.mean(Cluster_k,axis=0)
            
    return Labels, Centroids
    