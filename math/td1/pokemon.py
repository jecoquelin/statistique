#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 16 15:57:26 2022

@author: vcolomer
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#import math
#import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

#from sklearn.cluster import KMeans

pokemons = pd.read_csv("pokemon.csv")

"""
data = {
    'Age': [25, 47, 38],
    'Birth Year': [1995, 1973, 1982],
    'Graduation Year': [2016, 2000, 2005]        
}

df = pd.DataFrame(data)

mp_array = df.to_numpy()"""

def Centrereduire(T):
    transposeT = T.transpose()
    res = np.zeros((T.shape[1], T.shape[0]))
    
    for i in range(len(transposeT)):
        moy = np.average(transposeT[i])
        eT = np.std(transposeT[i])
        
        for j in range(len(transposeT[i])):
            res[i][j] = float(transposeT[i][j] - moy) / eT
            
    return res.transpose()

PokArray = pokemons._get_numeric_data().to_numpy()

PokArrayCR = Centrereduire(PokArray)
PokDF = pd.DataFrame(PokArrayCR)

Res=KMeans(n_clusters=4).fit(PokArrayCR[:,(2,4)])

def CoordCluster(X, Y, Res, i):
    values = list()
    for j in range(len(Res.labels_)):
        if(i == Res.labels_[j]):
            values.append([X[j], Y[j]])
            
    return np.array(values);

X = PokArrayCR[:,2]
Y = PokArrayCR[:,4]

c1 = CoordCluster(X, Y, Res, 0)
c2 = CoordCluster(X, Y, Res, 1)
c3 = CoordCluster(X, Y, Res, 2)
c4 = CoordCluster(X, Y, Res, 3)

plt.plot(c1[:, 0], c1[:, 1], '+', label="cluster 1")
plt.plot(c2[:, 0], c2[:, 1], '+', label="cluster 2")
plt.plot(c3[:, 0], c3[:, 1], '+', label="cluster 3")
plt.plot(c4[:, 0], c4[:, 1], '+', label="cluster 4")

plt.legend(loc=9)  # Affichage de la légende
plt.ylabel('Vitesse')  # Titre de l'axe y
plt.xlabel("Defense")  # Titre de l'axe x
plt.title("Les pokémons")  # Titre du graphique
plt.grid(True)  # Affichage de la grille
plt.show()  # Affichage d'une courbe
