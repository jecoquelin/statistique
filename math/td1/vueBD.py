#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 31 10:34:08 2022

@author: jecoquelin
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

########################
#      Question 7      #
########################

Vue_s1_origine = pd.read_csv("Vue_s1_origine.csv")

df=pd.DataFrame(Vue_s1_origine._get_numeric_data())

VueArray = df.to_numpy()


def CentreReduire(array):
    
    x,y = array.shape
    tab = np.zeros((x,y))
    moy = np.average(array,axis=0)
    ecartype = np.std(array,axis=0)
    for i in range(0,x):
        for j in range(0,y):
            tab[i][j] = (array[i][j] - moy[j]) / ecartype[j]
    return tab

tabIndex = ['moy_m1101','moy_m1102','moy_m1103','moy_m1104','moy_m1105','moy_m1106','moy_m1201','moy_m1202','moy_m1203','moy_m1204','moy_m1205','moy_m1206','moy_m1207']
VueArrayCR =pd.DataFrame(data = CentreReduire(VueArray),columns = tabIndex)
print("la vue array CR")
print(VueArrayCR)

#VueArrayCR = CentreReduire(VueArray)
#print("\nData frame centre Reduire")
#print(VueArrayCR)

########################
#      Question 8      #
########################



ensDonnées = KMeans(n_clusters=4).fit(VueArrayCR) #Créer un groupe de 4 cluster mais avec l'ensemble des données.
print(ensDonnées.cluster_centers_)



