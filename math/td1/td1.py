import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import warnings
warnings.filterwarnings("ignore",category = DeprecationWarning)

pokemons=pd.read_csv("pokemon.csv")


df=pd.DataFrame(pokemons._get_numeric_data())

PokArray = df.to_numpy()

def CentreReduire(array):
    
    x,y = array.shape
    tab = np.zeros( (x,y))
    moy = np.average(array,axis=0)
    ecartype = np.std(array,axis=0)
    for i in range(0,x):
        for j in range(0,y):
            tab[i][j] = (array[i][j] - moy[j]) / ecartype[j]
    return tab

tabIndex = ['Total','HP','Attack','Defense','Special Attack','Special Defense','Speed']
PokemonsCR =pd.DataFrame(data = CentreReduire(PokArray),columns = tabIndex)
PokArrayCR = CentreReduire(PokArray)
print("\nData frame centre Reduire")
print(PokemonsCR)

########################
#         TD1          #
########################


def graphique(Serie1,Serie2):
    
    plt.plot(Serie1,Serie2,'ro')

    plt.xlabel('Test')
    plt.show()


#Question 1:


X = PokemonsCR['Defense']
print("\n\n",X)

Y = PokemonsCR['Speed']
print("\n\n\n",Y)

graphique(X,Y)

Res=KMeans(n_clusters=8).fit(PokArrayCR[:,(2,4)])
print(Res.labels_)
#Question 2
speedDef=KMeans(n_clusters=4).fit(PokArrayCR[:,(5,6)])
print(speedDef.cluster_centers_)

#Question 3

def CoordCluster(X,Y,Res,i):
    num_cluster = Res.labels_
    xi = []
    yi = []
    for j in range (0,len(X)):
        if num_cluster[j] == i:
            xi.append(X[j])
            yi.append(Y[j])
    return np.array(xi),np.array(yi)


c,v = CoordCluster(X,Y,Res,3)
graphique(c,v)