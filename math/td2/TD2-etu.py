# -*- coding: utf-8 -*-
"""
Created on Mon May 23 16:48:03 2022

@author: tiphaine
"""

import pandas as pd
import numpy as np
import numpy.random as rd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


def Centreduire(T):
    TMoy=np.mean(T,axis=0)
    TEcart=np.std(T,axis=0)
    (n,p)=T.shape
    res=np.zeros((n,p)) 
    
    for j in range(p):
         res[:,j]=(T[:,j]-TMoy[j])/TEcart[j]
    return res


column_values=['MATH', 'PHYS', 'FRAN', 'ANGL']
index_values=['jean','alan','anni','moni','didi','andr','pier','brig','evel']
notes=np.array([[6.00, 6.00, 5.00, 5.50],
                [8.00, 8.00, 8.00, 8.00],
                [6.00, 7.00, 11.00, 9.50],
                [14.50, 14.50, 15.50, 15.00],
                [14.00, 14.00, 12.00, 12.50],
                [11.00, 10.00, 5.50, 7.00],
                [5.50, 7.00, 14.00, 11.50],
                [13.00, 12.50, 8.50, 9.50],
                [9.00, 9.50, 12.50, 12.00]])
notesCR=Centreduire(notes)

df = pd.DataFrame(data = notesCR, 
                  index = index_values, 
                  columns = column_values)

def graphique(Serie1,Serie2):
    
    plt.plot(Serie1,Serie2,'ro')
    for i, label in enumerate(index_values):
        plt.text(Serie1[i], Serie2[i],label)
    
    plt.xlabel('Notes math')
    plt.ylabel('Notes physiques')
    plt.show()


x = np.array(notesCR[:, 0])   #notes de math
y = np.array(notesCR[:, 1])   #notes de physiques

##question 2

graphique(x,y)

#question 3

tab = np.array(notesCR[:,(0,1)])
print(tab)

#question 4

