# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 15:22:58 2022

@author: tiphaine
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

def Centreduire(T):
    TMoy=np.mean(T,axis=0)
    TEcart=np.std(T,axis=0)
    (n,p)=T.shape
    res=np.zeros((n,p)) 
    
    for j in range(p):
         res[:,j]=(T[:,j]-TMoy[j])/TEcart[j]
    return res

####################### Représentation en dimension 2################

#Question1
VueDf=pd.read_csv("Vue_s1_origine1.csv")


#Question2
VueArray=VueDf._get_numeric_data().to_numpy()
VueArrayCR=Centreduire(VueArray)

#Question3
acp=PCA()
coord=acp.fit_transform(VueArrayCR)
#var=acp.explained_variance_ratio_
#print(acp.singular_values_)

#Question4
X=coord[:,0]
Y=coord[:,1]
plt.plot(X,Y,'o')

#Question5
for i in range(len(VueArray)):
    plt.text(coord[i,0],coord[i,1],VueDf['libelle_etablissement'][i])
    if VueDf['serie'][i]=='S':
        plt.plot(coord[i,0],coord[i,1],'bo')
    elif VueDf['serie'][i]=='STI2D':
        plt.plot(coord[i,0],coord[i,1],'go')
    else :
        plt.plot(coord[i,0],coord[i,1],'ro')   

################# Cercle des corrélations ###################


#Question7

print(np.corrcoef(VueArrayCR[:,0],coord[:,0]))
x0=np.corrcoef(VueArrayCR[:,0],coord[:,0])[0,1]
y0=np.corrcoef(VueArrayCR[:,0],coord[:,1])[0,1]

#Question8
plt.figure()
plt.plot([0,x0],[0,y0])

#Questions9&10
for i in range(VueArray.shape[1]):
    xi=np.corrcoef(VueArrayCR[:,i],coord[:,0])[0,1]
    yi=np.corrcoef(VueArrayCR[:,i],coord[:,1])[0,1]
    plt.plot([0,xi],[0,yi])
    plt.text(xi,yi,VueDf.columns[i+2])
        

#Question11
angles=np.linspace(0, 2*np.pi, 100)
plt.plot(np.cos(angles),np.sin(angles))


