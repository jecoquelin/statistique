#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 14 10:49:24 2022

@author: jecoquelin
"""

#Quetion 1:

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA
import warnings
warnings.filterwarnings("ignore",category = DeprecationWarning)

vue_s1=pd.read_csv("Vue_s1_origine1.csv")


VueDf=pd.DataFrame(vue_s1._get_numeric_data())

VueArray = VueDf.to_numpy()

#Question 2 :

def CentreReduire(array):
    
    x,y = array.shape
    tab = np.zeros( (x,y))
    moy = np.average(array,axis=0)
    ecartype = np.std(array,axis=0)
    for i in range(0,x):
        for j in range(0,y):
            tab[i][j] = (array[i][j] - moy[j]) / ecartype[j]
    return tab

VueArrayCR = CentreReduire(VueArray)

#Question 3:

acp=PCA()
coord= acp.fit_transform(VueArrayCR)

#Question 4:

X=coord[:,0]
Y=coord[:,1]
plt.figure()

plt.plot(X,Y,'o')
plt.xlabel('Composante principale 1')
plt.ylabel('Composante principale 2')

#Question 5:

for i in range(len(vue_s1['libelle_etablissement'])):
    plt.text(X[i],Y[i],vue_s1['libelle_etablissement'][i]) 

#Question 6:
TabZ = vue_s1['serie']
for i in range (len(TabZ)):
    if (TabZ[i] == "S"):
        plt.plot(X[i],Y[i],'oy')
    elif (TabZ[i] == "STI2D"):
        plt.plot(X[i],Y[i],'ob')
    else :
        plt.plot(X[i],Y[i],'or')
        
#Question 7:
print("COUCOU")
print(np.corrcoef(VueArrayCR[:,0],coord[:,0]))
x0=np.corrcoef(VueArrayCR[:,0],coord[:,0])[0,1]
y0=np.corrcoef(VueArrayCR[:,0],coord[:,1])[0,1]
        



