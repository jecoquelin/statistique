# -*- coding: utf-8 -*-
"""
Created on Mon May 23 16:48:03 2022

@author: tiphaine
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


def Centreduire(T):
    TMoy=np.mean(T,axis=0)
    TEcart=np.std(T,axis=0)
    (n,p)=T.shape
    res=np.zeros((n,p)) 
    
    for j in range(p):
         res[:,j]=(T[:,j]-TMoy[j])/TEcart[j]
    return res


column_values=['MATH', 'PHYS', 'FRAN', 'ANGL']
index_values=['jean','alan','anni','moni','didi','andr','pier','brig','evel']
notes=np.array([[6.00, 6.00, 5.00, 5.50],
                [8.00, 8.00, 8.00, 8.00],
                [6.00, 7.00, 11.00, 9.50],
                [14.50, 14.50, 15.50, 15.00],
                [14.00, 14.00, 12.00, 12.50],
                [11.00, 10.00, 5.50, 7.00],
                [5.50, 7.00, 14.00, 11.50],
                [13.00, 12.50, 8.50, 9.50],
                [9.00, 9.50, 12.50, 12.00]])
notesCR=Centreduire(notes)

df = pd.DataFrame(data = notes, 
                  index = index_values, 
                  columns = column_values)

############### ACP en dimension 2 ########################
######### Passer de 2 dimensions à 1 dimension ###########

#Question1
X=notesCR[:,0]
Y=notesCR[:,1]

plt.figure()
plt.axis([-2,2,-2,2])
plt.plot(X,Y,'o')
plt.xlabel('MATH')
plt.ylabel('PHYS')

#Question2
for i in range(len(index_values)):
    plt.text(X[i],Y[i],index_values[i])
    
#Question3
XY=notesCR[:,(0,1)]

#Question4
acp1=PCA()
coord1=acp1.fit_transform(XY)
#print(coord1)

#Nouveaux axes (pas demandé dans le TP)
NewAxes=acp1.components_
for i in range(2):
    [xi,yi]=NewAxes[i,:]
    plt.plot([0,xi],[0,yi])
    plt.text(xi,yi,'Composante'+str(i+1))

#Question5
X1=coord1[:,0]
Y1=coord1[:,1]
plt.figure()
plt.axis([-2,2,-2,2])
plt.plot(X1,Y1,'o')
plt.xlabel('Composante principale 1')
plt.ylabel('Composante principale 2')

#Question6
for i in range(len(index_values)):
    plt.text(X1[i],Y1[i],index_values[i]) 
   
#Anciens axes (pas demandé dans le TP)
OldAxes=np.linalg.inv(NewAxes)
for i in range(2):
    [xi,yi]=OldAxes[i,:]
    plt.plot([0,xi],[0,yi])
    plt.text(xi,yi,column_values[i])


############### ACP en dimension 3 #################
######### Passer de 3 dimensions à 2 dimensions ###########

#Question9
Z=notesCR[:,2]
fig=plt.figure()
fig=plt.axes(projection='3d')
fig.axes.set_xlim3d(left=-2, right=2) 
fig.axes.set_ylim3d(bottom=-2, top=2) 
fig.axes.set_zlim3d(bottom=-2, top=2)
fig.plot3D(X,Y,Z,'o')
fig.set_xlabel('MATH')
fig.set_ylabel('PHYS')
fig.set_zlabel('FRAN')
for i in range(len(index_values)):
    fig.text(X[i],Y[i],Z[i],index_values[i])
   
#Question10
XYZ=notesCR[:,(0,1,2)]

#Question11
acp2=PCA()    
coord2=acp2.fit_transform(XYZ)

#Nouveaux axes (pas demandé dans le TP)
NewAxes2=acp2.components_
for i in range(3):
    [xi,yi,zi]=NewAxes2[i,:]
    fig.plot3D([0,xi],[0,yi],[0,zi])
    fig.text(xi,yi,zi,'Composante'+str(i+1))

#Question12
X2=coord2[:,0]
Y2=coord2[:,1]
Z2=coord2[:,2]
fig2=plt.figure()
fig2=plt.axes(projection='3d')
fig2.axes.set_xlim3d(left=-2, right=2) 
fig2.axes.set_ylim3d(bottom=-2, top=2) 
fig2.axes.set_zlim3d(bottom=-2, top=2) 
fig2.plot3D(X2,Y2,Z2,'o')
fig2.set_xlabel('Composante principale 1')
fig2.set_ylabel('Composante principale 2')
fig2.set_zlabel('Composante principale 3')
for i in range(len(index_values)):
    fig2.text(X2[i],Y2[i],Z2[i],index_values[i])
    
#Anciens axes (pas demandé dans le TP)
OldAxes2=np.linalg.inv(NewAxes2)
for i in range(3):
    [xi,yi,zi]=OldAxes2[i,:]
    fig2.plot3D([0,xi],[0,yi],[0,zi])
    fig2.text(xi,yi,zi,column_values[i])  