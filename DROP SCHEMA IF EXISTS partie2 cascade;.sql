DROP SCHEMA IF EXISTS partie2 cascade;
CREATE SCHEMA partie2;
SET SCHEMA 'partie2';
DROP TABLE IF EXISTS _resultat;
DROP TABLE IF EXISTS _programme;
DROP TABLE IF EXISTS _inscription;
DROP TABLE IF EXISTS _sinscrire;
DROP TABLE IF EXISTS _postuler;
DROP TABLE IF EXISTS _module;
DROP TABLE IF EXISTS _semestre;
DROP TABLE IF EXISTS _etudiant;
DROP TABLE IF EXISTS _candidat;
DROP TABLE IF EXISTS _individu;
--Création de
CREATE TABLE _individu (
    id_individu SERIAL,
    nom VARCHAR NOT NULL,
    prenom VARCHAR NOT NULL,
    date_naissance DATE NOT NULL,
    code_postal VARCHAR NOT NULL,
    ville VARCHAR NOT NULL,
    sexe CHAR(1) NOT NULL,
    nationalité VARCHAR NOT NULL,
    INE VARCHAR NOT NULL,
    CONSTRAINT PK_individu PRIMARY KEY (id_individu)
);

CREATE TABLE _candidat (
    no_candidat SERIAL,
    classement VARCHAR NULL,
    boursier_lycée VARCHAR NOT NULL,
    profil_candidat VARCHAR NOT NULL,
    etablissement VARCHAR ,
    dept_etablissement VARCHAR ,
    ville_etablissement VARCHAR ,
    niveau_etude VARCHAR ,
    type_formation_prec VARCHAR NOT NULL,
    serie_prec VARCHAR NOT NULL,
dominante_prec VARCHAR NOT NULL,
specialite_prec VARCHAR NOT NULL,
LV1 VARCHAR ,
LV2 VARCHAR ,
CONSTRAINT PK_candidat
PRIMARY KEY (no_candidat)
);
CREATE TABLE _etudiant (
code_nip VARCHAR,
cat_socio_etu VARCHAR NOT NULL,
cat_socio_parent VARCHAR NOT NULL,
bourse_superieur BOOLEAN NOT NULL,
mention_bac VARCHAR ,
serie_bac VARCHAR ,
dominante_bac VARCHAR ,
specialite_bac VARCHAR ,
mois_annee_obtention_bac CHAR(7) ,
CONSTRAINT PK_etudiant
PRIMARY KEY (code_nip)
);
CREATE TABLE _semestre (
id_semestre SERIAL,
num_semestre CHAR(5) NOT NULL,
annee_univ CHAR(9) NOT NULL,
CONSTRAINT PK_semestre
PRIMARY KEY (id_semestre)
);
CREATE TABLE _module (
id_module CHAR(5),
libelle_module VARCHAR NOT NULL,
ue CHAR(2) NOT NULL,
CONSTRAINT PK_module
PRIMARY KEY (id_module)
);
CREATE TABLE _postuler(
no_candidat SERIAL,
id_individu INT NOT NULL,
CONSTRAINT PK_postuler
PRIMARY KEY (no_candidat),
CONSTRAINT _postuler_fk_candidat
FOREIGN KEY (no_candidat)
REFERENCES _candidat(no_candidat),
CONSTRAINT _postuler_fk_individu
FOREIGN KEY (id_individu)
REFERENCES _individu(id_individu)
);
CREATE TABLE _sinscrire(
id_individu INT,
code_nip VARCHAR NOT NULL,
CONSTRAINT PK_sinscrire
PRIMARY KEY (code_nip),
CONSTRAINT _sinscrire_fk__etudiant
FOREIGN KEY (id_individu)
REFERENCES _individu (id_individu),
CONSTRAINT _sinscrire_fk__individu
FOREIGN KEY (code_nip)
REFERENCES _etudiant (code_nip)
);
CREATE TABLE _inscription (
code_nip VARCHAR,
id_semestre SERIAL,
groupe_tp CHAR(2) NOT NULL,
amenagement_evaluation VARCHAR NOT NULL,
CONSTRAINT PK_inscription
PRIMARY KEY (code_nip,id_semestre),
CONSTRAINT _inscription_fk_etudiant
FOREIGN KEY (code_nip)
REFERENCES _etudiant(code_nip),
CONSTRAINT _inscription_fk_semestre
FOREIGN KEY (id_semestre)
REFERENCES _semestre(id_semestre)
);
CREATE TABLE _programme (
id_semestre SERIAL,
id_module CHAR(5),
coefficient NUMERIC NOT NULL,
CONSTRAINT PK_prgramme
PRIMARY KEY (id_semestre,id_module),
CONSTRAINT _programme_fk_semestre
FOREIGN KEY (id_semestre)
REFERENCES _semestre(id_semestre),
CONSTRAINT _programme_fk_module
FOREIGN KEY (id_module)
REFERENCES _module(id_module)
);
CREATE TABLE _resultat (
code_nip VARCHAR,
id_semestre SERIAL,
id_module CHAR(5),
moyenne NUMERIC NOT NULL,
CONSTRAINT PK_resultat
PRIMARY KEY (code_nip,id_semestre,id_module),
CONSTRAINT _resultat_fk_etudiant
FOREIGN KEY (code_nip)
REFERENCES _etudiant(code_nip),
CONSTRAINT _resultat_fk_semestre
FOREIGN KEY (id_semestre)
REFERENCES _semestre(id_semestre),
CONSTRAINT _resultat_fk_module
FOREIGN KEY (id_module)
REFERENCES _module(id_module)
);


no_candidat SERIAL,
classement VARCHAR NULL,                ->v_candidatures
boursier_lycée VARCHAR NOT NULL,        ->v_candidatures
profil_candidat VARCHAR NOT NULL,
etablissement VARCHAR ,
dept_etablissement VARCHAR ,
ville_etablissement VARCHAR ,
niveau_etude VARCHAR ,
type_formation_prec VARCHAR NOT NULL,
serie_prec VARCHAR NOT NULL,
dominante_prec VARCHAR NOT NULL,
specialite_prec VARCHAR NOT NULL,
LV1 VARCHAR ,
LV2 VARCHAR ,