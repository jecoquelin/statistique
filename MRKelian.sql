SET SCHEMA partie1 ;

CREATE TABLE _individu {
    id_individu     int,
    nom             varchar,
    prenom          varchar,
    date_naissance  date,
    code_postal     char(5),
    ville           varchar, 
    sexe            char(1),
    nationalité     varchar, 
    INE             char(11)
} ;

CREATE TABLE _etudiant {
    code_nip        varchar,
    cat_socio_etu   varchar, 
    cat_socio_parent varchar,
    bourse_superieur boolean,
    mention_bac     varchar(2),
    serie_bac       char(2),
    dominante_bac   varchar,
    specialite_bac  varchar, 
    mois_annee_obtention_bac    char(7)
} ;

/************************
 *      RELATIONS       *
 ************************/
CREATE TABLE _sinscrire {
    id_individu     int,
    code_nip        varchar
} ;

/********************************************
 *              ALTER TABLE                 *
 ********************************************/

/************************
 *      PRIMARY KEY     *
 ************************/

/*_sinscrire*/
ALTER TABLE _sinscrire  ADD CONSTRAINT  PK_sinscrire PRIMARY KEY (code_nip) ;


/************************
 *      FOREIGN KEY     *
 ************************/

 /*_individu*/
ALTER TABLE _individu   ADD CONSTRAINT  PK_individu PRIMARY KEY (id_individu);

/*_etudiant*/
ALTER TABLE _etudiant   ADD CONSTRAINT  PK_etudiant PRIMARY KEY (code_nip);

/*_sinscrire*/
ALTER TABLE _sinscrire  ADD CONSTRAINT  _sinscrire_fk__etudiant FOREIGN KEY (id_individu) REFERENCES _individu (id_individu) ;
ALTER TABLE _sinscrire  ADD CONSTRAINT  _sinscrire_fk__individu FOREIGN KEY (code_nip) REFERENCES _etudiant (code_nip) ;
