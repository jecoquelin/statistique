
/*  CE SCRIPT A POUR BUT DE PEUPLER UNE BASE DE DONNEES REALISTE QUI SERVIRA    */
    /*   LORS DES PROCHAINES SEANCES POUR LA REALISATION DE STATISTIQUE     */


/*******************************************************/
/*          REINITIALISATION DU SCHEMA ET TABLE        */
/*******************************************************/

DROP SCHEMA IF EXISTS partie2 cascade ;
CREATE SCHEMA partie2 ;
SET SCHEMA 'partie2';
 
DROP TABLE IF EXISTS _resultat;
DROP TABLE IF EXISTS _programme;
DROP TABLE IF EXISTS _inscription;
DROP TABLE IF EXISTS _sinscrire;
DROP TABLE IF EXISTS _postuler;
DROP TABLE IF EXISTS _module;
DROP TABLE IF EXISTS _semestre;
DROP TABLE IF EXISTS _etudiant;
DROP TABLE IF EXISTS _candidat;
DROP TABLE IF EXISTS _individu;
DROP TABLE IF EXISTS _tempprogramme ;
DROP TABLE IF EXISTS _tempppn ;
DROP TABLE IF EXISTS _tempcandidature;
DROP TABLE IF EXISTS _tempinscription;
DROP TABLE IF EXISTS _tempetudiant;
 


/****************************************/
/*        CREATION DES TABLES           */
/****************************************/

CREATE TABLE _individu (
    nom                         VARCHAR NOT NULL,
    prenom                      VARCHAR ,
    date_naissance              DATE NOT NULL,
    code_postal                 VARCHAR NOT NULL,
    ville                       VARCHAR NOT NULL, 
    sexe                        CHAR(1) NOT NULL,
    nationalité                 VARCHAR NOT NULL, 
    INE                         VARCHAR NOT NULL,
    CONSTRAINT PK_individu 
        PRIMARY KEY (INE)
);

 
CREATE TABLE _candidat (
    annee_candidature           VARCHAR,
    no_candidat                 INT,
    classement                  VARCHAR NULL,
    boursier_lycee              VARCHAR ,
    profil_candidat             VARCHAR NOT NULL,
    etablissement               VARCHAR ,
    dept_etablissement          VARCHAR ,
    ville_etablissement         VARCHAR ,
    niveau_etude                VARCHAR ,
    type_formation_prec         VARCHAR ,
    serie_prec                  VARCHAR ,
    dominante_prec              VARCHAR ,
    specialite_prec             VARCHAR ,
    LV1                         VARCHAR ,
    LV2                         VARCHAR ,
    CONSTRAINT PK_candidat 
        PRIMARY KEY (annee_candidature,no_candidat)
);
 
CREATE TABLE _etudiant (
    annee_univ                  VARCHAR ,
    no_candidat                 BIGINT ,
    cat_socio_etu               VARCHAR , 
    cat_socio_parent            VARCHAR ,
    bourse_superieur            VARCHAR ,
    mention_bac                 VARCHAR ,
    serie_bac                   VARCHAR ,
    dominante_bac               VARCHAR ,
    specialite_bac              VARCHAR , 
    mois_annee_obtention_bac    VARCHAR ,
    CONSTRAINT PK_etudiant 
        PRIMARY KEY (no_candidat,annee_univ)
);
 
CREATE TABLE _semestre (
   id_semestre                 SERIAL,
   num_semestre                CHAR(5) NOT NULL,
   annee_univ                  CHAR(9) NOT NULL,
   CONSTRAINT PK_semestre
       PRIMARY KEY (id_semestre)
);

CREATE TABLE _module (
   id_module                   VARCHAR,
   libelle_module              VARCHAR NOT NULL,
   ue                          VARCHAR NOT NULL,
   CONSTRAINT PK_module
       PRIMARY KEY (id_module)
);
 
CREATE TABLE _postuler(
    annee_candidature           VARCHAR,
    no_candidat                 INT,
    INE                         VARCHAR NOT NULL,
    CONSTRAINT PK_postuler 
        PRIMARY KEY (annee_candidature,no_candidat),
    CONSTRAINT _postuler_fk_candidat
        FOREIGN KEY (annee_candidature,no_candidat)
            REFERENCES _candidat(annee_candidature,no_candidat),
    CONSTRAINT _postuler_fk_individu
        FOREIGN KEY (INE)
            REFERENCES _individu(INE)
);
 
CREATE TABLE _sinscrire(
    INE                         VARCHAR,
    code_nip                    BIGINT NOT NULL,
    annee_univ                  VARCHAR NOT NULL,
    CONSTRAINT PK_sinscrire 
        PRIMARY KEY (code_nip,annee_univ),
    CONSTRAINT _sinscrire_fk__individu
        FOREIGN KEY (INE)
            REFERENCES _individu (INE),
    CONSTRAINT _sinscrire_fk__etudiant
        FOREIGN KEY (code_nip,annee_univ)
            REFERENCES _etudiant (no_candidat,annee_univ)
);
 
CREATE TABLE _inscription (
    code_nip                    BIGINT,
    annee_univ                  VARCHAR,
    id_semestre                 SERIAL,
    groupe_tp                   CHAR(2) NOT NULL,
    amenagement_evaluation      VARCHAR NOT NULL,
    CONSTRAINT PK_inscription 
        PRIMARY KEY (code_nip,id_semestre),
    CONSTRAINT _inscription_fk_etudiant
        FOREIGN KEY (code_nip,annee_univ)
            REFERENCES _etudiant(no_candidat,annee_univ),
    CONSTRAINT _inscription_fk_semestre
        FOREIGN KEY (id_semestre)
            REFERENCES _semestre(id_semestre)
);
 
CREATE TABLE _programme (
   id_semestre                 INTEGER,
   id_module                   CHAR(6),
   coefficient                 NUMERIC NOT NULL,
   CONSTRAINT PK_prgramme
       PRIMARY KEY (id_semestre,id_module),
   CONSTRAINT _programme_fk_semestre
       FOREIGN KEY (id_semestre)
           REFERENCES _semestre(id_semestre),
   CONSTRAINT _programme_fk_module
       FOREIGN KEY (id_module)
           REFERENCES _module(id_module)
);
 
CREATE TABLE _resultat (
   code_nip                    BIGINT,
   annee_univ                  VARCHAR,
   id_semestre                 SERIAL,
   id_module                   CHAR(5),
   moyenne                     NUMERIC NOT NULL,
   CONSTRAINT PK_resultat
       PRIMARY KEY (code_nip,id_semestre,id_module),
   CONSTRAINT _resultat_fk_etudiant
       FOREIGN KEY (code_nip, annee_univ)
           REFERENCES _etudiant(no_candidat, annee_univ),
   CONSTRAINT _resultat_fk_semestre
       FOREIGN KEY (id_semestre)
           REFERENCES _semestre(id_semestre),
   CONSTRAINT _resultat_fk_module
       FOREIGN KEY (id_module)
           REFERENCES _module(id_module)
);



/***********************************************/
/*	   CREATION DES TABLES TEMPORAIRES	CSV    */
/***********************************************/
 
/* IMPORT DU FICHIER CANDIDATURE DANS UNE TABLE TEMPORAIRE */
 
CREATE TABLE _tempcandidature(
 annee_candidature         VARCHAR,
 numero_candidat		       VARCHAR,
 classement                VARCHAR,
 nom                       VARCHAR,
 prenom                    VARCHAR,
 sexe                      VARCHAR,
 date_naissance            VARCHAR,
 nationalite               VARCHAR,
 code_postal               VARCHAR,
 ville                     VARCHAR,
 pays                      VARCHAR,
 boursier                  VARCHAR,
 profil_candidat           VARCHAR,
 numero_ine                VARCHAR,
 type_etablissement        VARCHAR,
 libelle_etablissement     VARCHAR,
 commune_etablissement     VARCHAR,
 departement_etablissement VARCHAR,
 pays_etablissement        VARCHAR,
 niveau_etudes_actuel      VARCHAR,
 type_formation            VARCHAR,
 serie                     VARCHAR,
 dominante                 VARCHAR,
 specialite                VARCHAR,
 lv1                       VARCHAR,
 lv2                       VARCHAR,
 diplome                   VARCHAR,
 diplome_serie             VARCHAR,
 diplome_dominante         VARCHAR,
 diplome_specialite        VARCHAR,
 diplome_mention           VARCHAR,
 diplome_mois_annee_obtention_bac  VARCHAR
);

WbImport   -file=/home/etuinfo/khamard/Documents/S2/sae2.04/seance2/data/v_candidatures.csv
           -header=true
           -delimiter=';'
           -schema=partie2
           -table=_tempcandidature
;
 
 
/* IMPORT DU FICHIER Inscription DANS UNE TABLE TEMPORAIRE */

CREATE TABLE _tempinscription(
annee_univ                 VARCHAR,
etape                      VARCHAR,
libel_etape                VARCHAR,
version_etape              INTEGER,
num_etudiant               BIGINT,
ine                        VARCHAR,
nom                        VARCHAR,
prenom                     VARCHAR,
date_naissance             VARCHAR,
sexe                       VARCHAR,
nationalite                VARCHAR,
code_postal                VARCHAR,
ville                      VARCHAR,
cat_socio_etu              VARCHAR,
cat_socio_parent           VARCHAR,
serie_bac                  VARCHAR,
mention_bac                VARCHAR,
mois_annee_obtention_bac   INT,
departement_bac            VARCHAR,
etablissement_bac          VARCHAR,
lib_etab_bac               VARCHAR,
situ_annee_prec            VARCHAR,
bourse_superieur           VARCHAR
);
 
WbImport   -file=/home/etuinfo/khamard/Documents/S2/sae2.04/seance2/data/v_inscriptions.csv
           -header=true
           -delimiter=';'
           -table=_tempinscription
           -schema=partie2
;


/* IMPORT DU FICHIER Programme DANS UNE TABLE TEMPORAIRE */

CREATE TABLE _tempprogramme(
   annee_univ          VARCHAR,
   num_semestre        VARCHAR,
   id_module           VARCHAR,
   coefficient         NUMERIC
);
 
WbImport    -file=/home/etuinfo/khamard/Documents/S2/sae2.04/seance2/data/v_programme.csv
           -header=true
           -delimiter=';'
           -table=_tempprogramme
           -schema=partie2
;           


/* IMPORT DU FICHIER PPN DANS UNE TABLE TEMPORAIRE */

CREATE TABLE _tempppn (
   id_module       VARCHAR,
   id_ue           VARCHAR,
   nom_module      VARCHAR,
   coef_module     NUMERIC,
   cm_module       INT,
   td_module       INT,
   tp_module       INT,
   id_semestre     CHAR(2)
);

WbImport   -file=/home/etuinfo/khamard/Documents/S2/sae2.04/seance2/data/ppn.csv
           -header=true
           -delimiter=';'
           -table=_tempppn
           -schema=partie2
;
 


/************************************************************************/
/*        CREATION DE TABLES TEMPORAIRES POUR LES TABLES FINALES        */
/************************************************************************/

/*IMPORTATION DES DONNES DANS LA CLASSE _TEMPSEMESTRE*/
CREATE TABLE _tempsemestre (
   id_semestre                 INTEGER,
   num_semestre                CHAR(5) NOT NULL,
   annee_univ                  CHAR(9) NOT NULL
);

WbImport  -file=/FILER/HOME/INFO/khamard/Documents/S2/sae2.04/seance2/data/v_programme.csv
          -header=true
          -delimiter=';'
          -table=_tempsemestre
          -schema=partie2
          -filecolumns=annee_univ,num_semestre
;


/*IMPORTATION DES DONNEES POUR LA CLASSE _TEMPRESULTAT*/
CREATE TABLE _tempresultat (
   code_nip                    VARCHAR,
   id_semestre                 SERIAL,
   id_module                   CHAR(5),
   moyenne                     NUMERIC NOT NULL
);



/*******************************************/
/*      PEUPLEMENT DES TABLES FINALES      */
/*******************************************/
 
/*TABLE _INDIVIDU*/
WbImport  -file=/home/etuinfo/khamard/Documents/S2/sae2.04/seance2/data/v_candidatures.csv
          -header=true
          -delimiter=';'
          -table=_individu
          -schema=partie2
          -filecolumns=$wb_skip$,$wb_skip$,$wb_skip$,nom,prenom,sexe,date_naissance,nationalité,code_postal,ville,$wb_skip$,$wb_skip$,$wb_skip$,INE
;

/*TABLE _CANDIDAT*/
WbImport -file=./data/v_candidatures.csv
         -header=true
         -delimiter=';'
         -table=_candidat
         -schema=partie2
         -filecolumns=annee_candidature,no_candidat,classement,$wb_skip$,$wb_skip$,$wb_skip$,$wb_skip$,$wb_skip$,$wb_skip$,$wb_skip$,$wb_skip$,boursier_lycee,profil_candidat,$wb_skip$,$wb_skip$,etablissement,ville_etablissement,dept_etablissement,$wb_skip$,niveau_etude,type_formation_prec,serie_prec,dominante_prec,specialite_prec,LV1,LV2;
UPDATE _candidat
SET boursier_lycee = REPLACE(boursier_lycee, 'Boursier de l''enseignement supérieur', 'Non Boursier');

/*TABLE _SEMESTRE*/
INSERT INTO _semestre(num_semestre,annee_univ) SELECT DISTINCT num_semestre,annee_univ FROM _tempsemestre;

/*TABLE _MODULE*/
WbImport  -file=/FILER/HOME/INFO/khamard/Documents/S2/sae2.04/seance2/data/ppn.csv
          -header=true
          -delimiter=';'
          -table=_module
          -schema=partie2
          -filecolumns=id_module,ue,libelle_module
;

/*TABLE _PROGRAMME*/
INSERT INTO _programme(id_semestre,id_module,coefficient)
SELECT id_semestre,id_module,coefficient FROM _tempprogramme
INNER JOIN _semestre ON _semestre.annee_univ=_tempprogramme.annee_univ AND _semestre.num_semestre=_tempprogramme.num_semestre;

/*TABLE _ETUDIANT */
/*Table étudiant en cour en de construction, pas fini*/
/*
INSERT INTO _etudiant
select 
  annee_univ,
  num_etudiant,
  cat_socio_etu,
  cat_socio_parent,
  bourse_superieur,
  mention_bac,
  serie_bac,
  dominante_bac,
  specialite_bac,
  mois_annee_obtention_bac
  from _tempinscription
  natural join _tempcandidature;
*/  


/****************************************************/
/*      SUPPRESSION DES TABLES TEMPORAIRES          */
/****************************************************/
DROP TABLE _tempcandidature;
DROP TABLE _tempinscription ;
DROP TABLE _tempsemestre;
DROP TABLE _tempprogramme;
DROP TABLE _tempppn;
DROP TABLE _tempresultat