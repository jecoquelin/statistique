SET SCHEMA partie1 ;

CREATE TABLE _individu {
    id_individu                 INT,
    nom                         VARCHAR,
    prenom                      VARCHAR,
    date_naissance              DATE,
    code_postal                 VARCHAR
    ville                       VARCHAR, 
    sexe                        CHAR(1),
    nationalité                 VARCHAR, 
    INE                         VARCHAR
} ;

CREATE TABLE _candidat{

    no_candidat                 INT,
    classement                  VARCHAR == NULL,
    boursier_lycée              VARCHAR,
    profil_candidat             VARCHAR,
    etablissement               VARCHAR,
    dept_etablissement          VARCHAR,
    ville_etablissement         VARCHAR,
    niveau_etude                VARCHAR,
    type_formation_prec         VARCHAR,
    serie_prec                  VARCHAR,
    dominante_prec              VARCHAR,
    specialite_prec             VARCHAR,
    LV1                         VARCHAR,
    LV2                         VARCHAR
};

CREATE TABLE _etudiant {
    code_nip                    VARCHAR,
    cat_socio_etu               VARCHAR, 
    cat_socio_parent            VARCHAR,
    bourse_superieur            BOOLEAN,
    mention_bac                 VARCHAR,
    serie_bac                   VARCHAR,
    dominante_bac               VARCHAR,
    specialite_bac              VARCHAR, 
    mois_annee_obtention_bac    CHAR(7)
} ;

CREATE TABLE _semestre {
    id_semestre                 INT,
    num_semestre                CHAR(5),
    annee_univ                  CHAR(9)
} 

CREATE TABLE _module{
    id_module                   CHAR(5),
    libelle_module              VARCHAR,
    ue                          CHAR(2)
}

CREATE TABLE _postuler{
    no_candidat                 INT,
    id_individu                 INT
};

CREATE TABLE _sinscrire {
    id_individu                 INT,
    code_nip                    VARCHAR
} ;


CREATE TABLE _inscription{
    code_nip                    VARCHAR,
    id_semestre                 INT,
    groupe_tp                   CHAR(2),
    amenagement_evaluation      VARCHAR
}



CREATE TABLE _programme{
    id_semestre                 INT,
    id_module                   CHAR(5),
    coefficient                 NUMERIC
}

CREATE TABLE _resultat{
    code_nip                    VARCHAR,
    id_semestre                 INT,
    id_module                   CHAR(5),
    moyenne                     NUMERIC
}

/**************************
*       ALTER TABLE       *
*       PRIMARY KEY       *
 *************************/
/* Primary key de _individu */
ALTER TABLE 
    _individu   
ADD     
    CONSTRAINT  PK_individu PRIMARY KEY (id_individu);

/* Primary key de _candidat */
ALTER TABLE 
    _candidat
ADD 
    CONSTRAINT PK_candidat PRIMARY KEY (no_candidat);

/* Primary key de _etudiant */
ALTER TABLE 
    _etudiant   
ADD 
    CONSTRAINT  PK_etudiant PRIMARY KEY (code_nip);

/* Primary key de _semestre */
ALTER TABLE
    _semestre
ADD
    CONSTRAINT PK_semestre PRIMARY KEY (id_semestre);

/* Primary key de _module */
ALTER TABLE
    _module
ADD 
    CONSTRAINT PK_module PRIMARY KEY (id_module);

/* Primary key de _postuler */
ALTER TABLE
    _postuler
ADD
    CONSTRAINT PK_postuler PRIMARY KEY (no_candidat);

/* Primary key de _sinscrire */
ALTER TABLE 
    _sinscrire  
ADD 
    CONSTRAINT PK_sinscrire PRIMARY KEY (code_nip) ;

/* Primary key de _inscription */
ALTER TABLE
    _inscription
ADD
    CONSTRAINT PK_inscription PRIMARY KEY (code_nip,id_semestre);

/* Primary key de _programme */
ALTER TABLE 
    _programme
ADD
    CONSTRAINT PK_prgramme PRIMARY KEY (id_semestre,id_module);

/* Primary key de _resultat */
ALTER TABLE
    _resultat
ADD
    CONSTRAINT PK_resultat PRIMARY KEY (code_nip,id_semestre,id_module);

/**************************
*       ALTER TABLE       *
*       FOREIGN KEY       *
 *************************/

/* Clé étrangère de la relation _postuler */
ALTER TABLE
    _postuler
ADD
    CONSTRAINT _postuler_fk_candidat FOREIGN KEY
    (no_candidat) REFERENCES _candidat(no_candidat);

ALTER TABLE 
    _postuler
ADD 
    CONSTRAINT _postuler_fk_individu FOREIGN KEY
    (id_individu) REFERENCES _individu(id_individu);

/* Clé étrangère de la relation _sinscrire  */
ALTER TABLE 
    _sinscrire  
ADD 
    CONSTRAINT  _sinscrire_fk__etudiant FOREIGN KEY (id_individu) REFERENCES _individu (id_individu) ;

ALTER TABLE 
    _sinscrire  
ADD 
    CONSTRAINT  _sinscrire_fk__individu FOREIGN KEY (code_nip) REFERENCES _etudiant (code_nip) ;

/* Clé étrangère de la relation _inscription */
ALTER TABLE
    _inscription
ADD
    CONSTRAINT _inscription_fk_etudiant FOREIGN KEY
    (code_nip) REFERENCES _etudiant(code_nip);

ALTER TABLE 
    _inscription
ADD
    CONSTRAINT _inscription_fk_semestre FOREIGN KEY
    (id_semestre) REFERENCES _semestre(id_semestre);

/* Clé étrangère de la relation _programme */
ALTER TABLE 
    _programme
ADD
    CONSTRAINT _programme_fk_semestre FOREIGN KEY
    (id_semestre) REFERENCES _semestre(id_semestre);

ALTER TABLE 
    _programme
ADD 
    CONSTRAINT _programme_fk_module FOREIGN KEY
    (id_module) REFERENCES _module(id_module);

/* Clé étrangère de la relation _resultat */
ALTER TABLE
    _resultat
ADD
    CONSTRAINT _resultat_fk_etudiant FOREIGN KEY
    (code_nip) REFERENCES _resultat(code_nip);

ALTER TABLE
    _resultat
ADD
    CONSTRAINT _resultat_fk_semestre FOREIGN KEY
    (id_semestre) REFERENCES _semestre(id_semestre);

ALTER TABLE
    _resultat
ADD
    CONSTRAINT _resultat_fk_module FOREIGN KEY
    (id_module) REFERENCES _resultat(id_module);